#include <Keypad.h>
#include <string.h>
#include <SPI.h>
#include <MFRC522.h>
#include <Servo.h>

#define RST_PIN         9           // Configurable, see typical pin layout above
#define SS_PIN          10          // Configurable, see typical pin layout above

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

const byte ROWS = 4; //number of rows on the keypad
const byte COLS= 4; //number of columns on the keypad

//keymap defines the key pressed according to the row and columns just as appears on the keypad
char hexaKeys[ROWS][COLS]=
{
{'1', '2', '3', 'A'},
{'4', '5', '6', 'B'},
{'7', '8', '9', 'C'},
{'*', '0', '#', 'D'}
};

//Code that shows the the keypad connections to the arduino terminals
byte rowPins[ROWS] = {5,4,3,2}; //Rows 0 to 3
byte colPins[COLS]= {9,8,7,6}; //Columns 0 to 3
const String passcode = "180296";
String enter = "";
//char hexaKeys[ROWS][COLS] = {
//  {'1', '2', '3'},
//  {'4', '5', '6'},
//  {'7', '8', '9'},
//  {'*', '0', '#'}
//};
//
//byte rowPins[ROWS] = {8, 7, 6, 5};
//byte colPins[COLS] = {4, 3, 2};
char check = '0';
int currentPos = 0;

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

Servo myservo;  // create servo object to control a servo
// twelve servo objects can be created on most boards

int pos = 0;    // variable to store the servo position

void setup() {
  SPI.begin();      // Initiate  SPI bus
  Serial.begin(9600);
  myservo.attach(14); 
  myservo.write(currentPos);
}

void loop() {
  if (check == '0') {
    RFIDScan();
    char customKey = customKeypad.getKey();
    if (customKey == '#') {
      if (passcode == enter) {
        check = '1';
        enter = "";
        Serial.println("Authorized access");
        openDoor();
      }
      else if (enter == "") {
        Serial.println("Closed");
        check = '0';
        closeDoor();
      }
      else {
        Serial.println("Incorrect. Please try again");
      }
      enter = "";
    }
    else if (customKey == '*') {
      enter.remove(enter.length() - 1);
      Serial.println(enter);
    }
    else if (customKey) {
      enter.concat(customKey);
      Serial.println(enter);
    }
  }

}

void RFIDScan() {
  mfrc522.PCD_Init();
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    return;
  }
  //Show UID on serial monitor
  Serial.print("UID tag :");
  String content = "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  Serial.print("Message : ");
  content.toUpperCase();
  if (content.substring(1) == "80 C3 E2 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("Authorized access");
    check = '1';
    Serial.println();
    openDoor();
  }

  else   {
    Serial.println(" Access denied");
    check = '0';

  }
}

void openDoor(){
  for (pos = currentPos; pos <= 150; pos += 1) { // goes from 0 degrees to 180 degrees
    // in steps of 1 degree
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  
  currentPos = 150;
  delay(10000);
  closeDoor();
}

void closeDoor(){
  for (pos = currentPos; pos >= 0; pos -= 1) { // goes from 180 degrees to 0 degrees
    myservo.write(pos);              // tell servo to go to position in variable 'pos'
    delay(15);                       // waits 15ms for the servo to reach the position
  }
  currentPos = 0;
}
