// IMPORTANT: Adafruit_TFTLCD LIBRARY MUST BE SPECIFICALLY
// CONFIGUGREEN FOR EITHER THE TFT SHIELD OR THE BREAKOUT BOARD.
// SEE RELEVANT COMMENTS IN Adafruit_TFTLCD.h FOR SETUP.
//

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_TFTLCD.h> // Hardware-specific library
#include <TouchScreen.h>
#include <string.h>

// The control pins for the LCD can be assigned to any digital or
// analog pins...but we'll use the analog pins as this allows us to
// double up the pins with the touch screen (see the TFT paint example).
#define LCD_CS A3 // Chip Select goes to Analog 3
#define LCD_CD A2 // Command/Data goes to Analog 2
#define LCD_WR A1 // LCD Write goes to Analog 1
#define LCD_RD A0 // LCD Read goes to Analog 0

#define LCD_RESET A4 // Can alternately just connect to Arduino's reset pin

// When using the BREAKOUT BOARD only, use these 8 data lines to the LCD:
// For the Arduino Uno, Duemilanove, Diecimila, etc.:
//   D0 connects to digital pin 8  (Notice these are
//   D1 connects to digital pin 9   NOT in order!)
//   D2 connects to digital pin 2
//   D3 connects to digital pin 3
//   D4 connects to digital pin 4
//   D5 connects to digital pin 5
//   D6 connects to digital pin 6
//   D7 connects to digital pin 7
// For the Arduino Mega, use digital pins 22 through 29
// (on the 2-row header at the end of the board).

// Assign human-readable names to some common 16-bit color values:
#define  BLACK   0x0000
#define BLUE    0x001F
#define GREEN     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF


// Color definitions
#define ILI9341_BLACK       0x0000      /*   0,   0,   0 */
#define ILI9341_NAVY        0x000F      /*   0,   0, 128 */
#define ILI9341_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define ILI9341_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define ILI9341_MAROON      0x7800      /* 128,   0,   0 */
#define ILI9341_PURPLE      0x780F      /* 128,   0, 128 */
#define ILI9341_OLIVE       0x7BE0      /* 128, 128,   0 */
#define ILI9341_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define ILI9341_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define ILI9341_BLUE        0x001F      /*   0,   0, 255 */
#define ILI9341_GREEN       0x07E0      /*   0, 255,   0 */
#define ILI9341_CYAN        0x07FF      /*   0, 255, 255 */
#define ILI9341_GREEN         0xF800      /* 255,   0,   0 */
#define ILI9341_MAGENTA     0xF81F      /* 255,   0, 255 */
#define ILI9341_YELLOW      0xFFE0      /* 255, 255,   0 */
#define ILI9341_WHITE       0xFFFF      /* 255, 255, 255 */
#define ILI9341_ORANGE      0xFD20      /* 255, 165,   0 */
#define ILI9341_GREENYELLOW 0xAFE5      /* 173, 255,  47 */
#define ILI9341_PINK        0xF81F

/******************* UI details */
#define BUTTON_X 40
#define BUTTON_Y 100
#define BUTTON_W 60
#define BUTTON_H 30
#define BUTTON_SPACING_X 20
#define BUTTON_SPACING_Y 20
#define BUTTON_TEXTSIZE 2

// text box where numbers go
#define TEXT_X 10
#define TEXT_Y 10
#define TEXT_W 220
#define TEXT_H 50
#define TEXT_TSIZE 3
#define TEXT_TCOLOR ILI9341_MAGENTA
// the data (phone #) we store in the textfield
#define TEXT_LEN 12
char textfield[TEXT_LEN + 1] = "";
uint8_t textfield_i = 0;

#define YP A3  // must be an analog pin, use "An" notation!
#define XM A2  // must be an analog pin, use "An" notation!
#define YM 9   // can be a digital pin
#define XP 8   // can be a digital pin

#define TS_MINX 100
#define TS_MAXX 920

#define TS_MINY 70
#define TS_MAXY 900
// We have a status line for like, is FONA working

#include <MCUFRIEND_kbv.h>
MCUFRIEND_kbv tft;
// If using the shield, all control and data lines are fixed, and
// a simpler declaration can optionally be used:
// Adafruit_TFTLCD tft;

TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);
// If using the shield, all control and data lines are fixed, and
// a simpler declaration can optionally be used:
// Adafruit_TFTLCD tft;

Adafruit_GFX_Button buttons[15];
/* create 15 buttons, in classic candybar phone style */

#include <SPI.h>
#include <MFRC522.h>
#define BOXSIZE 80
char check = '0';
int pos = 0;

String comboStr = "";
String clearStr = "";

#define SS_PIN 10
#define RST_PIN 9
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

void setup()
{ // Initiate a serial communication
  SPI.begin();      // Initiate  SPI bus
  //mfrc522.PCD_Init();   // Initiate MFRC522
  Serial.begin(9600);
  //  for (pos = 0; pos <= 180; pos += 2) { // goes from 0 degrees to 180 degrees
  //    // in steps of 1 degree
  //    myservo.write(pos);              // tell servo to go to position in variable 'pos'
  //    delay(15);                       // waits 15ms for the servo to reach the position
  //  }

  tft.reset();
  uint16_t identifier = tft.readID();
  identifier = 0x7783;
  tft.begin(identifier); // SDFP5408
  tft.setRotation(0); // if you rotate the screen you have to rotate touch position
  tft.fillScreen(BLACK);
  startScreen();
  waitOneTouch();
  buildButtons();
  //pinMode(13, OUTPUT);

  tft.setCursor (10, 255);
  tft.setTextWrap(false);

}

#define MINPRESSURE 10
#define MAXPRESSURE 1000

void loop()
{
  RFIDScan();
  if (check == '1') {
    authorisedAccess();
  }


  digitalWrite(13, HIGH);
  TSPoint p = ts.getPoint();
  digitalWrite(13, LOW);

  // if sharing pins, you'll need to fix the directions of the touchscreen pins
  //pinMode(XP, OUTPUT);
  pinMode(XM, OUTPUT);
  pinMode(YP, OUTPUT);
  //pinMode(YM, OUTPUT);

  // we have some minimum pressure we consider 'valid'
  // pressure of 0 means no pressing!
  Serial.println(comboStr.length());
  if (comboStr.length() >= 6) {
    if (comboStr == "180296"){
      authorisedAccess();
    }
    else {
      comboStr = clearStr;
      btnCheck();
    }
  }
  if (p.z > MINPRESSURE && p.z < MAXPRESSURE) {
    // scale from 0->1023 to tft.width
    p.x = map(p.x, TS_MINX, TS_MAXX, tft.width(), 0);
    p.y = map(p.y, TS_MINY, TS_MAXY, 0, tft.height());

    /*
      Serial.print("("); Serial.print(p.x);
      Serial.print(", "); Serial.print(p.y);
      Serial.println(")");
    */
    if (p.y < BOXSIZE) {
      if (p.x > BOXSIZE * 2) {
        // if touch detected within box. button1  what code to carry out goes next....
        btn3();
        comboStr += '3';
      }
      else if (p.x > BOXSIZE) {
        btn2();
        comboStr += '2';
      }
      else if (p.x > 0) {
        btn1();
        comboStr += '1';
      }
    }// END OF ROW 1.......................................................TOUCH BUTTON
    else if (p.y < BOXSIZE * 2) {
      if (p.x > BOXSIZE * 2) {
        btn6();
        comboStr += '6';
      }
      else if (p.x > BOXSIZE) {
        btn5();
        comboStr += '5';
      }
      else if (p.x > 0) {
        btn4();
        //  btnPushCounter++;
        comboStr += '4';
      }
    }// END OF ROW 2...............................................TOUCH BUTTON
    else if (p.y < BOXSIZE * 3) {
      if (p.x > BOXSIZE * 2) {
        btn9();
        comboStr += '9';
      }
      else if (p.x > BOXSIZE) {
        btn8();
        comboStr += '8';
      }
      else if (p.x > 0) {
        btn7();
        comboStr += '7';
      }
    }// END OF ROW 3...............................................TOUCH BUTTON
    else if (p.y < BOXSIZE * 4) {
      if (p.x > BOXSIZE * 2) {
        btn0();
        comboStr += '0';
      }
      else if (p.x > 0) {
        btnCheck();
        Serial.println(comboStr);
        //  Serial.println(btnPushCounter);
        if (comboStr == "180296") { // if count match......change here for diffrent combo

          //          for (pos = 180; pos >= 0; pos -= 2) { // goes from 180 degrees to 0 degrees
          //            myservo.write(pos);              // tell servo to go to position in variable 'pos'
          //            delay(15);   // waits 15ms for the servo to reach the position
          //          }
          authorisedAccess();
        }
        else if (comboStr == 0) {

        }

        comboStr = clearStr;
      }
    }
  }//  MINPRESSURE MAXPRESSURE END
}// END VOID LOOP

// Wait one touch
TSPoint waitOneTouch() {
  // wait 1 touch to exit function
  TSPoint p;
  do {
    p = ts.getPoint();
    pinMode(XM, OUTPUT); //Pins configures again for TFT control
    pinMode(YP, OUTPUT);
  } while ((p.z < MINPRESSURE ) || (p.z > MAXPRESSURE));
  return p;
}

void authorisedAccess() {
  check = '0';
  drawBorder();
  tft.setTextWrap(true);
  tft.setCursor (18, 50);
  tft.setTextSize (3);
  tft.setTextColor(GREEN);
  tft.println("Tap to close");
  waitOneTouch();
  //          for (pos = 0; pos <= 180; pos += 2) { // goes from 0 degrees to 180 degrees
  //            // in steps of 1 degree
  //            myservo.write(pos);              // tell servo to go to position in variable 'pos'
  //            delay(15);                       // waits 15ms for the servo to reach the position
  //          }

  buildButtons();
  comboStr = clearStr;
}
// Draw a border
void drawBorder () {
  uint16_t width = tft.width() - 1;
  uint16_t height = tft.height() - 1;
  uint8_t border = 10;
  tft.fillScreen(GREEN);
  tft.fillRect(border, border, (width - border * 2), (height - border * 2), BLACK);
}

//.......................button box build void....................
void buildButtons() {
  tft.fillScreen(BLACK);
  //......... button1  recktangles....
  tft.fillRect(0, 0, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(5, 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (30, 30);
  tft.println(1);
  //......... button 1 recktangles....
  //......... button 2  recktangles....
  tft.fillRect(BOXSIZE, 0, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE + 5, 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (110, 30);
  tft.println(2);
  //......... button 2 recktangles....
  //......... button 3 recktangles....
  tft.fillRect(BOXSIZE * 2, 0, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE * 2 + 5, 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (190, 30);
  tft.println(3);
  //......... button3 recktangles....
  //......... button 4 recktangles....
  tft.fillRect(0, BOXSIZE, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(5, BOXSIZE + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (30, 110);
  tft.println(4);
  //......... button4 recktangles....
  //......... button 5 recktangles....
  tft.fillRect(BOXSIZE, BOXSIZE, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE + 5, BOXSIZE + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (110, 110);
  tft.println(5);
  //......... button5 recktangles....
  //......... button 6 recktangles....
  tft.fillRect(BOXSIZE * 2, BOXSIZE, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE * 2 + 5, BOXSIZE + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (190, 110);
  tft.println(6);
  //......... button6 recktangles....
  //......... button 7 recktangles....
  tft.fillRect(0, BOXSIZE * 2, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(5, BOXSIZE * 2 + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (30, 190);
  tft.println(7);
  //......... button7 recktangles....
  //......... button 8 recktangles....
  tft.fillRect(BOXSIZE, BOXSIZE * 2, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE + 5, BOXSIZE * 2 + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (110, 190);
  tft.println(8);
  //......... button8 recktangles....
  //......... button 9 recktangles....
  tft.fillRect(BOXSIZE * 2, BOXSIZE * 2, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE * 2 + 5, BOXSIZE * 2 + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (190, 190);
  tft.println(9);

  //......... button0 recktangles....
  tft.fillRect(BOXSIZE * 2, BOXSIZE * 3, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE * 2 + 5, BOXSIZE * 3 + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (190, 270);
  tft.println(0);

  //......... button check recktangles....
  tft.fillRect(0, BOXSIZE * 3, 160, BOXSIZE, BLACK);
  tft.fillRect( 5, BOXSIZE * 3 + 5, 150, BOXSIZE - 10, GREEN);
  tft.setTextSize (2);
  tft.setTextColor(BLACK);
  tft.setCursor (40, 290);
  tft.println("Check");
  ///set cersor for tft.print
  tft.setCursor (10, 255);
  tft.setTextWrap(false);

  //......... button0 recktangles....
  tft.fillRect(BOXSIZE * 2, BOXSIZE * 3, BOXSIZE, BOXSIZE, BLACK);
  tft.fillRect(BOXSIZE * 2 + 5, BOXSIZE * 3 + 5, BOXSIZE - 10, BOXSIZE - 10, GREEN);
  tft.setTextSize (4);
  tft.setTextColor(BLACK);
  tft.setCursor (190, 270);
  tft.println(0);
}

//........................................button touch void...............
void btn0() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(0);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(BOXSIZE * 2,  BOXSIZE * 3, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(BOXSIZE * 2, BOXSIZE * 3, BOXSIZE, BOXSIZE, BLACK);

}

void btn1() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(1);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(0,  0, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(0,  0, BOXSIZE, BOXSIZE, BLACK);
}

void btn2() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(2);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(BOXSIZE,  0, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(BOXSIZE,  0, BOXSIZE, BOXSIZE, BLACK);
}
void btn3() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(3);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(BOXSIZE * 2,  0, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(BOXSIZE * 2,  0, BOXSIZE, BOXSIZE, BLACK);
}
void btn4() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(4);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(0,  BOXSIZE, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(0,  BOXSIZE, BOXSIZE, BOXSIZE, BLACK);
}
void btn5() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(5);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(BOXSIZE,  BOXSIZE, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(BOXSIZE,  BOXSIZE, BOXSIZE, BOXSIZE, BLACK);
}
void btn6() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(6);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(BOXSIZE * 2,  BOXSIZE, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(BOXSIZE * 2,  BOXSIZE, BOXSIZE, BOXSIZE, BLACK);
}
void btn7() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(7);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(0,  BOXSIZE * 2, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(0,  BOXSIZE * 2, BOXSIZE, BOXSIZE, BLACK);
}
void btn8() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(8);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(BOXSIZE,  BOXSIZE * 2, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(BOXSIZE,  BOXSIZE * 2, BOXSIZE, BOXSIZE, BLACK);
}
void btn9() {
  tft.setTextSize (3);
  tft.setTextColor(BLACK);
  tft.print(9);
  tft.fillRect(BOXSIZE * 3 - 8, BOXSIZE * 3 + 5, BOXSIZE + 2, BOXSIZE - 10, GREEN);
  tft.drawRect(BOXSIZE * 2,  BOXSIZE * 2, BOXSIZE, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(BOXSIZE * 2,  BOXSIZE * 2, BOXSIZE, BOXSIZE, BLACK);
}
void btnCheck() {
  tft.drawRect(0,  BOXSIZE * 3, BOXSIZE * 2, BOXSIZE, WHITE);
  delay(0500);
  tft.drawRect(0,  BOXSIZE * 3, BOXSIZE * 2, BOXSIZE, BLACK);
  delay(0500);
  tft.fillRect(0, BOXSIZE * 3, 160, BOXSIZE, BLACK);
  tft.fillRect( 5, BOXSIZE * 3 + 5, 150, BOXSIZE - 10, GREEN);
  tft.setTextSize (2);
  tft.setTextColor(BLACK);
  tft.setCursor (40, 290);
  tft.println("Check");
  tft.setCursor (10, 255);
  tft.setTextWrap(true);
}

void startScreen() {
  // Initial screen
  drawBorder();
  tft.setTextWrap(false);
  tft.setCursor (55, 50);
  tft.setTextSize (3);
  tft.setTextColor(GREEN);
  tft.println("Button");
  tft.setCursor (65, 85);
  tft.println("Action");
  tft.setCursor (55, 150);
  tft.setTextSize (2);
  tft.setTextColor(GREEN);
  tft.println("Sinical");
  tft.setCursor (80, 250);
  tft.setTextSize (1);
  tft.setTextColor(GREEN);
  tft.println("Touch to proceed");


}

void RFIDScan() {
  mfrc522.PCD_Init();
  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent())
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial())
  {
    return;
  }
  //Show UID on serial monitor
  Serial.print("UID tag :");
  String content = "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++)
  {
    Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
    Serial.print(mfrc522.uid.uidByte[i], HEX);
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  Serial.print("Message : ");
  content.toUpperCase();
  if (content.substring(1) == "80 C3 E2 2B") //change here the UID of the card/cards that you want to give access
  {
    Serial.println("Authorized access");
    check = '1';
    Serial.println();
    delay(100);
  }

  else   {
    Serial.println(" Access denied");
    check = '0';
    delay(100);
  }
}
