/* www.learningbuz.com */
/*Impport following Libraries*/
#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>

const String passcode = "180296";
String enter = "";
String encrypt = "";

const byte numRows = 4; //number of rows on the keypad
const byte numCols = 4; //number of columns on the keypad
//I2C pins declaration
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

//keymap defines the key pressed according to the row and columns just as appears on the keypad
char keymap[numRows][numCols] =
{
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};

//Code that shows the the keypad connections to the arduino terminals
byte rowPins[numRows] = {5, 4, 3, 2}; //Rows 0 to 3
byte colPins[numCols] = {9, 8, 7, 6}; //Columns 0 to 3

//initializes an instance of the Keypad class
Keypad myKeypad = Keypad(makeKeymap(keymap), rowPins, colPins, numRows, numCols);

void setup()
{
  Serial.begin(9600);
  lcd.begin(16, 2); //Defining 16 columns and 2 rows of lcd display
  lcd.backlight();//To Power ON the back light
  //lcd.backlight();// To Power OFF the back light

}

void loop()
{
  char customKey = myKeypad.getKey();
  if (customKey == '#') {
      if (passcode == enter) {
        enter = "";
        encrypt = "";
        lcd.setCursor(0,1);
        lcd.print("Authorized access");
        delay(1000);
        lcd.clear();
      }
      else if (enter == "") {
        lcd.setCursor(0,1);
        lcd.print("Closed");
        delay(1000);
        lcd.clear();
      }
      else {
        lcd.setCursor(0,1);
        lcd.print("Access denied");
        delay(1000);
        lcd.clear();
      }
      enter = "";
      encrypt = "";
    }
    else if (customKey == '*') {
      lcd.clear();
      lcd.setCursor(0,0);
      enter.remove(enter.length() - 1);
      encrypt.remove(enter.length() - 1);
      lcd.print(encrypt);
    }
    else if (customKey) {
      lcd.setCursor(0,0);
      enter.concat(customKey);
      encrypt.concat('*');
      lcd.print(encrypt);
    }
  //Write your code
//  lcd.setCursor(0, 0); //Defining positon to write from first row,first column .
//  lcd.print(" Tech Maker "); //You can write 16 Characters per line .
//  delay(1000);//Delay used to give a dynamic effect
//  lcd.setCursor(0, 1); //Defining positon to write from second row,first column .
//  lcd.print("Like | Share");
//  delay(8000);
//
//  lcd.clear();//Clean the screen
//  lcd.setCursor(0, 0);
//  lcd.print(" SUBSCRIBE ");
//  lcd.setCursor(0, 1);
//  lcd.print(" TECH MAKER ");
//  delay(8000);
}
